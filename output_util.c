// TODO Use rule `space` (must be implicitly declared by default) to know what a space is
static inline int check_spaces(const char **src)
{
	if(!lexer_space(**src))
		return 0;
	while(lexer_space(**src))
		++(*src);
	return 1;
}

static inline int check_char(const char **src, const char c)
{
	if(**src == 'c')
	{
		++(*src);
		return 1;
	}
	return 0;
}

static inline int check_range(const char **src, const char from, const char to)
{
	if(**src >= from && **src <= to)
	{
		++(*src);
		return 1;
	}
	return 0;
}

static inline int check_string(const char **src, const char *str)
{
	size_t len;

	len = strlen(str);
	if(strncmp(*src, str, len) == 0)
	{
		*src += len;
		return 1;
	}
	return 0;
}

static inline void skip_spaces(const char **src)
{
	while(lexer_space(**src))
		++(*src);
}

static token_t *new_token(const char **src)
{
	// TODO
	(void) src;
}

void tokens_free(token_t *t)
{
	token_t *next;

	while(t)
	{
		next = t->next;
		free((void *) t);
		t = next;
	}
}

token_t *lexer(const char *src)
{
	token_t *tokens = NULL, *last_token = NULL, *t;

	if(!src)
		return NULL;
	while(*src)
	{
		skip_spaces(&src);
		if(!(t = new_token(&src)))
		{
			tokens_free(tokens);
			return NULL;
		}
		if(last_token)
		{
			last_token->next = t;
			last_token = t;
		}
		else
			last_token = tokens = t;
	}
}

// TODO Parser utils
