#include <parser_gen.h>

static int check_duplicate(rule_t *r)
{
	while(r)
	{
		if(get_rule(r->next, r->name))
		{
			DUPLICATE_RULE(r->name);
			return 0;
		}
		r = r->next;
	}
	return 1;
}

static int check_rule_calls(rule_t *rules, ast_t *node, const int node_assign)
{
	const char *n;
	rule_t *r;

	while(node)
	{
		if(node->type == RULE_CALL_N)
		{
			if(!(n = strndup(node->token->data, node->token->data_size)))
				return 0;
			if(!(r = get_rule(rules, n)))
			{
				UNDECLARED_RULE(n);
				return 0;
			}
			if(!node_assign && r->node_assign)
			{
				CANT_CALL(n);
				return 0;
			}
			free((void *) n);
		}
		else if(!check_rule_calls(rules, node->children, node_assign))
			return 0;
		node = node->next;
	}
	return 1;
}

int check_rules(rule_t *rules)
{
	rule_t *r;

	if(!check_duplicate(rules))
		return 0;
	r = rules;
	while(r)
	{
		if(!check_rule_calls(rules, r->node, r->node_assign))
			return 0;
		r = r->next;
	}
	return 1;
}
