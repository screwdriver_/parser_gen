#include <parser_gen.h>

static rule_t *new_rule(ast_t *ast)
{
	rule_t *r;
	token_t *name_token, *assign_token;

	if(ast->type != RULE_N)
		goto parse_error;
	if(!(name_token = ast->token) || name_token->type != NAME_T)
		goto parse_error;
	if(!(assign_token = name_token->next) || (assign_token->type != ASSIGN_T
		&& assign_token->type != NODE_ASSIGN_T))
		goto parse_error;
	if(!(r = mem_alloc(sizeof(rule_t)))
		|| !(r->name = strndup(ast->token->data, ast->token->data_size)))
	{
		free((void *) r);
		return NULL;
	}
	r->node_assign = (assign_token->type == NODE_ASSIGN_T);
	r->node = ast;
	return r;

parse_error:
	// TODO Parse error
	return NULL;
}

rule_t *get_rules(ast_t *ast)
{
	rule_t *r, *rules = NULL, *last = NULL;

	if(!ast)
		return NULL;
	if(ast->type != ROOT_N)
	{
		// TODO Error
		return NULL;
	}
	ast = ast->children;
	while(ast)
	{
		if(!(r = new_rule(ast)))
		{
			free_rules(rules);
			return NULL;
		}
		if(last)
		{
			last->next = r;
			last = r;
		}
		else
			last = rules = r;
		ast = ast->next;
	}
	return rules;
}

rule_t *get_rule(rule_t *r, const char *name)
{
	while(r)
	{
		if(strcmp(r->name, name) == 0)
			return r;
		r = r->next;
	}
	return NULL;
}

size_t count_rules(rule_t *r, const int node_assign)
{
	size_t n = 0;

	while(r)
	{
		if(r->node_assign == node_assign)
			++n;
		r = r->next;
	}
	return n;
}

void print_rules(rule_t *rule)
{
	while(rule)
	{
		printf("-> %s\n", rule->name);
		rule = rule->next;
	}
}

void free_rules(rule_t *rule)
{
	rule_t *next;

	while(rule)
	{
		next = rule->next;
		free((void *) rule->name);
		free((void *) rule);
		rule = next;
	}
}
