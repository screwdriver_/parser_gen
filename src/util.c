#include <parser_gen.h>

void *mem_alloc(const size_t n)
{
	void *ptr;

	if((ptr = malloc(n)))
		bzero(ptr, n);
	if(errno)
		dprintf(2, "Memory allocation failed!\n");
	return ptr;
}

const char *read_file(const char *path)
{
	int fd;
	size_t len;
	char *buffer;

	if((fd = open(path, O_RDONLY)) < 0)
	{
		// TODO Error
		return NULL;
	}
	len = lseek(fd, 0, SEEK_END);
	lseek(fd, 0, SEEK_SET);
	if((buffer = mem_alloc(len + 1)))
		read(fd, buffer, len); // TODO Check for error
	close(fd);
	return buffer;
}

void print_tabs(const int fd, size_t n)
{
	while(n-- > 0)
		write(fd, "\t", 1);
}

void put_char(const int fd, const char c)
{
	write(fd, &c, 1);
}

void put_nbr(const int fd, const int n)
{
	if(n < 0)
		put_char(fd, '-');
	if(n > 9 || n < -9)
		put_nbr(fd, n / 10);
	put_char(fd, '0' + abs(n % 10));
}

void put_str(const int fd, const char *c)
{
	write(fd, c, strlen(c));
}

const char *strtoup(const char *str)
{
	char *buffer;
	size_t len;
	size_t i = 0;

	if(!str || !(buffer = strdup(str)))
		return NULL;
	len = strlen(str);
	while(i < len)
	{
		buffer[i] = toupper(buffer[i]);
		++i;
	}
	return buffer;
}
