#include <parser_gen.h>

ast_t *parse_rule(token_t **tok)
{
	ast_t *ast;

	if(!(ast = new_node(RULE_N, *tok)))
		goto fail;
	if((*tok)->type != NAME_T)
	{
		EXPECTED_NAME();
		goto fail;
	}
	*tok = (*tok)->next;
	if(!*tok)
	{
		UEOF();
		goto fail;
	}
	if((*tok)->type != ASSIGN_T && (*tok)->type != NODE_ASSIGN_T)
	{
		EXPECTED_ASSIGN();
		goto fail;
	}
	*tok = (*tok)->next;
	while(*tok && (*tok)->type != END_T)
		if(!(ast->children = parse_andor(tok)))
			goto fail;
	if(*tok && (*tok)->type == END_T)
		*tok = (*tok)->next;
	return ast;

fail:
	free_ast(ast);
	return NULL;
}
