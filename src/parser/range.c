#include <parser_gen.h>

ast_t *parse_range(token_t **tok)
{
	ast_t *ast, *c0, *c1;

	if(!(c0 = new_node(SCALAR_T, *tok))
		|| !(ast = new_node(RANGE_N, (*tok)->next)))
	{
		free_ast(c0);
		return NULL;
	}
	if(!(*tok = (*tok)->next->next) || (*tok)->type != SCALAR_T)
	{
		free_ast(c0);
		free_ast(ast);
		EXPECTED_OPERAND();
	}
	if(!(c1 = new_node(SCALAR_T, *tok)))
	{
		free_ast(c0);
		free_ast(ast);
		return NULL;
	}
	*tok = (*tok)->next;
	c0->next = c1;
	ast->children = c0;
	return ast;
}
