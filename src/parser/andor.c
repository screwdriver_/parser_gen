#include <parser_gen.h>

ast_t *parse_andor(token_t **tok)
{
	ast_t *n0 = NULL, *ast = NULL, *n1;
	int or;

	if(!(n0 = parse_concat(tok)))
		return NULL;
	if(!*tok || (*tok)->type == END_T || is_brace_close((*tok)->type))
		return n0;
	or = ((*tok)->type == OR_T);
	if(!(ast = new_node(ANDOR_N, *tok)))
		goto fail;
	if(or)
		*tok = (*tok)->next;
	ast->children = n0;
	if(!(n1 = parse_andor(tok)))
	{
		if(or)
			goto fail;
	}
	insert_next(n0, n1);
	return ast;

fail:
	free_ast(n0);
	free_ast(ast);
	return NULL;
}
