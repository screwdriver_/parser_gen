#include <parser_gen.h>

// TODO Exit code
// TODO Position on error messages

ast_t *new_node(const ast_type_t type, token_t *tok)
{
	ast_t *ast;

	if(!(ast = mem_alloc(sizeof(ast_t))))
		return NULL;
	ast->type = type;
	ast->token = tok;
	return ast;
}

void insert_next(ast_t *node, ast_t *n)
{
	if(!node || !n)
		return;
	while(node->next)
		node = node->next;
	node->next = n;
}

int is_brace_close(const token_type_t type)
{
	return (type == RBRACE_T || type == RSBRACE_T || type == RCBRACE_T);
}

ast_t *parser(token_t *tok)
{
	ast_t *ast, *rule;

	if(!tok)
		return NULL;
	if(!(ast = new_node(ROOT_N, tok)))
		goto fail;
	while(tok)
	{
		if(!(rule = parse_rule(&tok)))
			goto fail;
		if(!ast->children)
			ast->children = rule;
		else
			insert_next(ast->children, rule);
	}
	return ast;

fail:
	free_ast(ast);
	return NULL;
}

static void print_ast_(ast_t *ast, const size_t n)
{
	if(!ast)
		return;
	while(ast)
	{
		print_tabs(0, n);
		printf("- %i: %.*s\n", ast->type,
			(int) ast->token->data_size, ast->token->data);
		print_ast_(ast->children, n + 1);
		ast = ast->next;
	}
}

void print_ast(ast_t *ast)
{
	print_ast_(ast, 0);
}

void free_ast(ast_t *ast)
{
	ast_t *next;

	if(!ast)
		return;
	while(ast)
	{
		next = ast->next;
		free_ast(ast->children);
		free((void *) ast);
		ast = next;
	}
}
