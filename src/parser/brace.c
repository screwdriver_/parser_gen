#include <parser_gen.h>

// TODO Clean
ast_t *parse_brace(token_t **tok)
{
	ast_t *ast;
	token_type_t begin_type, end_type;

	if(!*tok)
	{
		UEOF();
		return NULL;
	}
	begin_type = (*tok)->type;
	if(begin_type == LBRACE_T)
	{
		ast = new_node(GROUP_N, *tok);
		end_type = RBRACE_T;
	}
	else if(begin_type == LSBRACE_T)
	{
		ast = new_node(OPTIONAL_GROUP_N, *tok);
		end_type = RSBRACE_T;
	}
	else if(begin_type == LCBRACE_T)
	{
		ast = new_node(REPETITION_GROUP_N, *tok);
		end_type = RCBRACE_T;
	}
	else
	{
		EXPECTED_OPERAND();
		return NULL;
	}
	*tok = (*tok)->next;
	if(!ast || !(ast->children = parse_andor(tok)))
	{
		free_ast(ast);
		return NULL;
	}
	if(!*tok || (*tok)->type != end_type)
	{
		UNEXPECTED();
		free_ast(ast);
		return NULL;
	}
	*tok = (*tok)->next;
	return ast;
}
