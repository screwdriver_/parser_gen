#include <parser_gen.h>

typedef struct
{
	const char *str;
	token_type_t type;
} token_pair_t;

static token_pair_t tokens[] = {
	{"=", ASSIGN_T},
	{":=", NODE_ASSIGN_T},
	{"|", OR_T},
	{",", CONCAT_T},
	{"-", RANGE_T},
	{";", END_T},
	{"(", LBRACE_T},
	{")", RBRACE_T},
	{"[", LSBRACE_T},
	{"]", RSBRACE_T},
	{"{", LCBRACE_T},
	{"}", RCBRACE_T}
};

static void skip_spaces(const char **src)
{
	while(isspace(**src))
		++(*src);
}

static token_pair_t *get_type(const char *src)
{
	size_t i = 0;

	while(i < sizeof(tokens) / sizeof(token_pair_t))
	{
		if(strncmp(tokens[i].str, src, strlen(tokens[i].str)) == 0)
			return tokens + i;
		++i;
	}
	return NULL;
}

static const char *get_name_end(const char *src)
{
	while(isalnum(*src) || *src == '_')
		++src;
	return src;
}

static const char *get_char_end(const char *src)
{
	size_t n = 0;

	++src;
	while(*src && *src != '\'')
	{
		++src;
		++n;
	}
	if(n != 1)
	{
		INVALID_CHAR();
		return NULL;
	}
	if(*src == '\'')
		++src;
	return src;
}

static const char *get_string_end(const char *src)
{
	++src;
	while(*src && *src != '"')
	{
		if(src[0] == '\\' && src[1])
			src += 2;
		else
			++src;
	}
	if(!*src)
	{
		UEOF();
		return NULL;
	}
	++src;
	return src;
}

static const char *get_digit_end(const char *src)
{
	if(*src == '+' || *src == '-')
		++src;
	if(src[0] == '0' && src[1] == 'x')
		src += 2;
	if(!isdigit(*src))
	{
		INVALID_VALUE();
		return NULL;
	}
	while(*src && isdigit(*src))
		++src;
	return src;
}

static const char *get_token_end(token_t *tok, const char *src)
{
	if(isalpha(*src))
	{
		tok->type = NAME_T;
		return get_name_end(src);
	}
	else if(*src == '\'')
	{
		tok->type = SCALAR_T;
		return get_char_end(src);
	}
	else if(*src == '"')
	{
		tok->type = STRING_T;
		return get_string_end(src);
	}
	else if(isdigit(src[0])
		|| ((src[0] == '+' || src[0] == '-') && isdigit(src[1])))
	{
		tok->type = SCALAR_T;
		return get_digit_end(src);
	}
	UNEXPECTED();
	return NULL;
}

static token_t *handle_token(const char **src)
{
	const char *begin;
	token_t *tok;
	token_pair_t *pair;

	begin = *src;
	if(!(tok = mem_alloc(sizeof(token_t))))
		return NULL;
	tok->data = *src;
	if((pair = get_type(*src)))
	{
		tok->type = pair->type;
		*src += strlen(pair->str);
	}
	else
		*src = get_token_end(tok, *src);
	if(*src)
		tok->data_size = *src - begin;
	else
	{
		free((void *) tok);
		return NULL;
	}
	return tok;
}

token_t *lexer(const char *src)
{
	token_t *tokens = NULL, *last_token = NULL, *t;

	while(src && *src)
	{
		skip_spaces(&src);
		if(!*src)
			break;
		if(!(t = handle_token(&src)))
		{
			free_tokens(tokens);
			return NULL;
		}
		if(last_token)
		{
			last_token->next = t;
			last_token = t;
		}
		else
			last_token = tokens = t;
	}
	return tokens;
}

void print_tokens(token_t *t)
{
	while(t)
	{
		printf("- %i: %.*s\n", t->type, (int) t->data_size, t->data);
		t = t->next;
	}
}

void free_tokens(token_t *t)
{
	token_t *next;

	if(!t)
		return;
	while(t)
	{
		next = t->next;
		free((void *) t);
		t = next;
	}
}
