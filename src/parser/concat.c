#include <parser_gen.h>

ast_t *parse_concat(token_t **tok)
{
	ast_t *n0 = NULL, *ast = NULL, *n1;

	if(!(n0 = parse_operand(tok)))
		goto fail;
	if(!*tok || (*tok)->type == END_T || is_brace_close((*tok)->type))
		return n0;
	if((*tok)->type != CONCAT_T)
		return n0;
	if(!(ast = new_node(CONCAT_N, *tok)))
		return NULL;
	ast->children = n0;
	*tok = (*tok)->next;
	if(!(n1 = parse_operand(tok)))
		goto fail;
	insert_next(n0, n1);
	return ast;

fail:
	free_ast(n0);
	free_ast(ast);
	return NULL;
}
