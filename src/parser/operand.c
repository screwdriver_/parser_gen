#include <parser_gen.h>

ast_t *parse_operand(token_t **tok)
{
	token_t *t;

	if(!*tok)
	{
		UEOF();
		return NULL;
	}
	if((*tok)->type == NAME_T)
	{
		t = *tok;
		*tok = (*tok)->next;
		return new_node(RULE_CALL_N, t);
	}
	else if((*tok)->type == SCALAR_T
		&& (*tok)->next && (*tok)->next->type == RANGE_T)
		return parse_range(tok);
	else if((*tok)->type == SCALAR_T || (*tok)->type == STRING_T)
	{
		t = *tok;
		*tok = (*tok)->next;
		return new_node(VALUE_N, t);
	}
	return parse_brace(tok);
}
