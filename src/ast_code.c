#include <parser_gen.h>

// TODO Cleanup
// TODO `return 1;` if optional group is last
// TODO Fix lambda ids

static void write_ast_code_(const int fd, const char *rule_name, ast_t *ast,
	const size_t level, const int or);

static void scope_begin(const int fd, const size_t level, const int stack)
{
	print_tabs(fd, level);
	put_str(fd, "{\n");
	if(stack)
	{
		print_tabs(fd, level + 1);
		put_str(fd, "const char *s = *src;\n");
	}
}

static void scope_end(const int fd, const size_t level, const int stack)
{
	if(stack)
	{
		print_tabs(fd, level + 1);
		put_str(fd, "*src = s;\n");
	}
	print_tabs(fd, level);
	put_str(fd, "}\n");
}

static void add_return(const int fd, ast_t *ast,
	const size_t level, const int or)
{
	if(!ast->next || or)
	{
		print_tabs(fd, level);
		put_str(fd, "return 1;\n");
	}
}

// TODO Adapt to node_assigns
static void write_rule_call(const int fd, ast_t *ast,
	const size_t level, const int or)
{
	print_tabs(fd, level);
	put_str(fd, "if(lexer_");
	write(fd, ast->token->data, ast->token->data_size);
	put_str(fd, "(src))\n");
	add_return(fd, ast, level + 1, or);
}

static void write_value(const int fd, ast_t *ast,
	const size_t level, const int or)
{
	print_tabs(fd, level);
	if(ast->token->data[0] == '"')
	{
		put_str(fd, "if(check_string(src, ");
		write(fd, ast->token->data, ast->token->data_size);
		put_str(fd, "))\n");
	}
	else
	{
		put_str(fd, "if(check_char(src, ");
		write(fd, ast->token->data, ast->token->data_size);
		put_str(fd, "))\n");
	}
	add_return(fd, ast, level + 1, or);
}

static void write_range(const int fd, ast_t *ast,
	const size_t level, const int or)
{
	ast_t *c0, *c1;

	// TODO Sanity check?
	c0 = ast->children;
	c1 = ast->children->next;
	print_tabs(fd, level);
	put_str(fd, "if(check_range(src, ");
	write(fd, c0->token->data, c0->token->data_size);
	put_str(fd, ", ");
	write(fd, c1->token->data, c1->token->data_size);
	put_str(fd, "))\n");
	add_return(fd, ast, level + 1, or);
}

static void write_andor(const int fd, const char *rule_name, ast_t *ast,
	const size_t level)
{
	int stack;

	if(ast->children && !ast->children->next)
	{
		write_ast_code_(fd, rule_name, ast->children, level, 1);
		return;
	}
	if(ast->token->type == OR_T)
	{
		write_ast_code_(fd, rule_name, ast->children, level, 1);
		write_ast_code_(fd, rule_name, ast->children->next, level, 1);
	}
	else
	{
		stack = (ast->next != NULL);
		write_ast_code_(fd, rule_name, ast->children, level, 0);
		scope_begin(fd, level, stack);
		print_tabs(fd, level + 1);
		put_str(fd, "if(check_spaces(src))\n");
		scope_begin(fd, level + 1, stack);
		write_ast_code_(fd, rule_name, ast->children->next, level + 2, 0);
		scope_end(fd, level + 1, stack);
		scope_end(fd, level, stack);
	}
}

static void write_concat(const int fd, const char *rule_name, ast_t *ast,
	const size_t level)
{
	int stack;

	if(ast->children && !ast->children->next)
	{
		write_ast_code_(fd, rule_name, ast->children, level, 1);
		return;
	}
	stack = (ast->next != NULL);
	write_ast_code_(fd, rule_name, ast->children, level, 0);
	scope_begin(fd, level, stack);
	write_ast_code_(fd, rule_name, ast->children->next, level + 1, 0);
	scope_end(fd, level, stack);
}

static void write_group(const int fd, ast_t *ast,
	const size_t level, const int or)
{
	print_tabs(fd, level);
	put_str(fd, "if(lambda");
	put_nbr(fd, get_lambda_id(ast));
	put_str(fd, "(src))\n");
	add_return(fd, ast, level + 1, or);
}

static void write_optional_group(const int fd, ast_t *ast,
	const size_t level, const int or)
{
	print_tabs(fd, level);
	put_str(fd, "lambda");
	put_nbr(fd, get_lambda_id(ast));
	put_str(fd, "(src);\n");
	add_return(fd, ast, level, or);
}

static void write_repetition_group(const int fd, ast_t *ast,
	const size_t level, const int or)
{
	print_tabs(fd, level);
	put_str(fd, "while(lambda");
	put_nbr(fd, get_lambda_id(ast));
	put_str(fd, "(src))\n");
	print_tabs(fd, level + 1);
	put_str(fd, ";\n");
	add_return(fd, ast, level, or);
}

static void write_ast_code_(const int fd, const char *rule_name, ast_t *ast,
	const size_t level, const int or)
{
	if(ast && ast->type == RULE_N)
		ast = ast->children;
	if(!ast)
	{
		print_tabs(fd, level);
		put_str(fd, "return 1;\n");
		return;
	}
	switch(ast->type)
	{
		case RULE_CALL_N:
		{
			write_rule_call(fd, ast, level, or);
			break;
		}

		case VALUE_N:
		{
			write_value(fd, ast, level, or);
			break;
		}

		case RANGE_N:
		{
			write_range(fd, ast, level, or);
			break;
		}

		case ANDOR_N:
		{
			write_andor(fd, rule_name, ast, level);
			break;
		}

		case CONCAT_N:
		{
			write_concat(fd, rule_name, ast, level);
			break;
		}

		case GROUP_N:
		{
			write_group(fd, ast, level, or);
			break;
		}

		case OPTIONAL_GROUP_N:
		{
			write_optional_group(fd, ast, level, or);
			break;
		}

		case REPETITION_GROUP_N:
		{
			write_repetition_group(fd, ast, level, or);
			break;
		}

		default: {}
	}
}

// TODO Handle nodes created in lambdas
static void write_lambdas(const int fd, rule_t *rule, ast_t *ast)
{
	ast_t *n;

	n = ast;
	while(n)
	{
		if(n->type == GROUP_N || n->type == OPTIONAL_GROUP_N
			|| n->type == REPETITION_GROUP_N)
		{
			put_str(fd, "int lambda");
			put_nbr(fd, get_lambda_id(n));
			put_str(fd, "(const char **src)\n{\n");
			put_str(fd, "\tconst char *s = *src;\n");
			write_ast_code_(fd, rule->name, n->children, 1, 0);
			put_str(fd, "\t*src = s;\n");
			put_str(fd, "\treturn 0;\n");
			put_str(fd, "}\n\n");
		}
		write_lambdas(fd, rule, n->children);
		n = n->next;
	}
}

void write_ast_code(const int fd, rule_t *rule)
{
	if(fd < 0)
		return;
	write_lambdas(fd, rule, rule->node->children);
	put_str(fd, "int lexer_");
	put_str(fd, rule->name);
	put_str(fd, "(const char **src)\n{\n");
	if(!rule->node)
	{
		put_str(fd, "\t(void) src;\n\treturn 1;\n");
		return;
	}
	put_str(fd, "\tconst char *s = *src;\n");
	write_ast_code_(fd, rule->name, rule->node, 1, 0);
	put_str(fd, "\t*src = s;\n");
	put_str(fd, "\treturn 0;\n");
	put_str(fd, "}\n\n");
}
