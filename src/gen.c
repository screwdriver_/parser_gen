#include <parser_gen.h>

// TODO Handle upper/lower case rule names

static const char *INCLUDES = "#include <stdlib.h>\n#include <string.h>\n\n";
static const char *TOKEN_ENUM_HEAD = "typedef enum\n{\n";
static const char *TOKEN_ENUM_FOOT = "} token_type_t;\n";
static const char *NODE_ENUM_HEAD = "typedef enum\n{\n";
static const char *NODE_ENUM_FOOT = "} node_type_t;\n";
static const char *STRUCTS = "typedef struct token\n\
{\n\
	struct token *next;\n\
\n\
	token_type_t type;\n\
	const char *data;\n\
	size_t data_size;\n\
} token_t;\n\
\n\
typedef struct node\n\
{\n\
	struct node *next;\n\
\n\
	node_type_t type;\n\
	token_t *token;\n\
} node_t;\n";

int gen_parser(const char *file, const char *output)
{
	const char *buffer;
	token_t *tokens = NULL;
	ast_t *ast = NULL;
	rule_t *rules = NULL;

	if(!output)
		output = "parser_out.c";
	if(!(buffer = read_file(file)))
		goto fail;
	if(!(tokens = lexer(buffer)))
		goto fail;
	// print_tokens(tokens);
	if(!(ast = parser(tokens)))
		goto fail;
	// print_ast(ast);
	if(!(rules = get_rules(ast)))
		goto fail;
	// print_rules(rules);
	if(!check_rules(rules))
		goto fail;
	write_output(ast, rules, output);
	free_rules(rules);
	free_ast(ast);
	free_tokens(tokens);
	free((void *) buffer);
	return 0;

fail:
	free_rules(rules);
	free_ast(ast);
	free_tokens(tokens);
	free((void *) buffer);
	return 1;
}

static void write_includes(const int fd)
{
	put_str(fd, INCLUDES);
}

static void write_enum_vals(const int fd, rule_t *r, const int node_assign)
{
	size_t count;
	const char *enum_name;

	count = count_rules(r, node_assign);
	while(r)
	{
		if(r->node_assign == node_assign)
		{
			if(!(enum_name = strtoup(r->name)))
				return;
			put_char(fd, '\t');
			put_str(fd, enum_name);
			free((void *) enum_name);
			put_str(fd, "_T");
			if(--count > 0)
				put_str(fd, ",\n");
			else
				break;
		}
		r = r->next;
	}
	put_char(fd, '\n');
}

static void write_enums(const int fd, rule_t *rules)
{
	put_str(fd, TOKEN_ENUM_HEAD);
	write_enum_vals(fd, rules, 0);
	put_str(fd, TOKEN_ENUM_FOOT);
	put_char(fd, '\n');
	put_str(fd, NODE_ENUM_HEAD);
	write_enum_vals(fd, rules, 1);
	put_str(fd, NODE_ENUM_FOOT);
}

static void write_prototypes(const int fd, rule_t *rules)
{
	lambda_t *l;
	rule_t *r;

	l = get_lambdas();
	while(l)
	{
		put_str(fd, "int lambda");
		put_nbr(fd, l->id);
		put_str(fd, "(const char **src);\n");
		l = l->next;
	}
	r = rules;
	while(r)
	{
		if(!r->node_assign)
		{
			put_str(fd, "int lexer_");
			put_str(fd, r->name);
			put_str(fd, "(const char **src);\n");
		}
		r = r->next;
	}
	put_char(fd, '\n');
	r = rules;
	while(r)
	{
		if(r->node_assign)
		{
			put_str(fd, "node_t *parser_");
			put_str(fd, r->name);
			put_str(fd, "(token_t **t);\n");
		}
		r = r->next;
	}
}

static void write_token_checks(const int fd, rule_t *r)
{
	while(r)
	{
		if(!r->node_assign)
			write_ast_code(fd, r);
		r = r->next;
	}
}

static void write_utils(const int fd)
{
	static const char *buff = NULL;

	if(!buff)
		buff = read_file("output_util.c"); // TODO Error handling
	put_str(fd, buff);
}

// TODO Check errno to see if an error happened for each step
void write_output(ast_t *ast, rule_t *rules, const char *output)
{
	int fd;

	remove(output);
	if((fd = open(output, O_WRONLY | O_CREAT, 0644)) < 0)
	{
		dprintf(2, "Failed to open/create file `%s`!\n", output);
		goto end;
	}
	prepare_lambdas(ast);
	write_includes(fd);
	write_enums(fd, rules);
	put_char(fd, '\n');
	put_str(fd, STRUCTS);
	put_char(fd, '\n');
	write_prototypes(fd, rules);
	put_char(fd, '\n');
	write_utils(fd);
	write_token_checks(fd, rules);
	put_char(fd, '\n');
	// TODO Create parser
	close(fd);

end:
	reset_lambdas();
}
