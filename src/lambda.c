#include <parser_gen.h>

static size_t curr_id = 0;
static lambda_t *lambdas = NULL;

static void insert_lambda(ast_t *node)
{
	lambda_t *l;

	if(!(l = mem_alloc(sizeof(lambda_t))))
		return;
	l->node = node;
	l->id = curr_id++;
	l->next = lambdas;
	lambdas = l;
}

void prepare_lambdas(ast_t *ast)
{
	while(ast)
	{
		if(ast->type == GROUP_N || ast->type == OPTIONAL_GROUP_N
			|| ast->type == REPETITION_GROUP_N)
			insert_lambda(ast);
		prepare_lambdas(ast->children);
		ast = ast->next;
	}
}

lambda_t *get_lambdas(void)
{
	return lambdas;
}

size_t get_lambda_id(ast_t *node)
{
	lambda_t *l;

	if(!node)
		return 0;
	l = lambdas;
	while(l)
	{
		if(l->node == node)
			return l->id;
		l = l->next;
	}
	return 0;
}

void reset_lambdas(void)
{
	lambda_t *l, *next;

	l = lambdas;
	while(l)
	{
		next = l->next;
		free((void *) l);
		l = next;
	}
	curr_id = 0;
	lambdas = NULL;
}
