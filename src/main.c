#include <parser_gen.h>

static void print_usage(void)
{
	dprintf(2, "usage: parser_gen <file> [output]\n");
}

int main(int argc, char **argv)
{
	if(argc <= 1)
	{
		print_usage();
		return 0;
	}
	return gen_parser(argv[1], argv[2]);
}
