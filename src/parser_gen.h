#ifndef PARSER_GEN
# define PARSER_GEN

# include <ctype.h>
# include <errno.h>
# include <fcntl.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <unistd.h>

// TODO Print position of error
# define UEOF()				dprintf(2, "Unexpected end-of-file!\n")
# define UNEXPECTED()		dprintf(2, "Unexpected character!\n")
# define INVALID_CHAR()		dprintf(2, "Invalid character!\n")
# define INVALID_VALUE()	dprintf(2, "Invalid value!\n")
# define EXPECTED_NAME()	dprintf(2, "Expected name!\n")
# define EXPECTED_ASSIGN()	dprintf(2, "Expected assignement!\n")
# define EXPECTED_OPERAND()	dprintf(2, "Expected operand!\n")

# define DUPLICATE_RULE(name)	dprintf(2, "Duplicate rule `%s`\n", (name))
# define UNDECLARED_RULE(name)	dprintf(2, "Undeclared rule `%s`!\n", (name))
# define CANT_CALL(name)		dprintf(2,\
	"Can't call node rule `%s` from non node rule!\n", (name))

typedef enum
{
	NAME_T = 0,
	ASSIGN_T = 1,
	NODE_ASSIGN_T = 2,
	SCALAR_T = 3,
	STRING_T = 4,
	OR_T = 5,
	CONCAT_T = 6,
	RANGE_T = 7,
	END_T = 8,
	LBRACE_T = 9,
	RBRACE_T = 10,
	LSBRACE_T = 11,
	RSBRACE_T = 12,
	LCBRACE_T = 13,
	RCBRACE_T = 14
} token_type_t;

typedef struct token
{
	struct token *next;

	token_type_t type;
	const char *data;
	size_t data_size;
} token_t;

typedef enum
{
	ROOT_N = 0,
	RULE_N = 1,
	RULE_CALL_N = 2,
	VALUE_N = 3,
	RANGE_N = 4,
	ANDOR_N = 5,
	CONCAT_N = 6,
	GROUP_N = 7,
	OPTIONAL_GROUP_N = 8,
	REPETITION_GROUP_N = 9
} ast_type_t;

typedef struct ast
{
	struct ast *children;
	struct ast *next;

	ast_type_t type;
	token_t *token;
} ast_t;

typedef struct rule
{
	struct rule *next;

	const char *name;
	int node_assign;
	ast_t *node;
} rule_t;

typedef struct lambda
{
	struct lambda *next;

	ast_t *node;
	size_t id;
} lambda_t;

void *mem_alloc(size_t n);
const char *read_file(const char *path);
void print_tabs(int fd, size_t n);
void put_char(int fd, char c);
void put_nbr(int fd, int n);
void put_str(int fd, const char *c);
const char *strtoup(const char *str);

token_t *lexer(const char *src);
void print_tokens(token_t *t);
void free_tokens(token_t *t);

ast_t *parser(token_t *tok);
ast_t *parse_andor(token_t **tok);
ast_t *parse_range(token_t **tok);
ast_t *parse_brace(token_t **tok);
ast_t *parse_operand(token_t **tok);
ast_t *parse_concat(token_t **tok);
ast_t *parse_andor(token_t **tok);
ast_t *parse_rule(token_t **tok);

ast_t *new_node(const ast_type_t type, token_t *tok);
void insert_next(ast_t *node, ast_t *n);
int is_brace_close(const token_type_t type);
void print_ast(ast_t *ast);
void free_ast(ast_t *ast);

rule_t *get_rules(ast_t *ast);
rule_t *get_rule(rule_t *r, const char *name);
size_t count_rules(rule_t *r, const int node_assign);
void print_rules(rule_t *rule);
void free_rules(rule_t *rule);

int check_rules(rule_t *r);

int gen_parser(const char *file, const char *output);
void write_output(ast_t *ast, rule_t *rules, const char *output);

void prepare_lambdas(ast_t *ast);
lambda_t *get_lambdas(void);
size_t get_lambda_id(ast_t *node);
void reset_lambdas(void);

void write_ast_code(int fd, rule_t *rule);

#endif
